# Component: Backend Web Partner

The component is made for being a parent of all backend web services.

## Packaging

pom

## Description

The project inherits from backend-parent (compilation and code quality tools) and adds base spring starters like:

- spring-cloud-starter-sleuth
- spring-boot-starter-test
- de.flapdoodle.embed.mongo - for testing
- mapstruct and lombok (as dependency; base configuration is in backend-parent)

and some custom starters shared along all webservices:
- db-spring-boot-starter - connection to database (Mongo or CosmosDB)
- security-spring-boot-starter - it consist of spring-boot-starter-webflux and configuration for JWT authentication

It uses backend-bom project to manage versions of all dependencies.

## Creating local release

```
mvn clean install
```

## Maven
```
    <parent>
         <groupId>com.jkarkoszka.getjabbed</groupId>
        <artifactId>backend-web-parent</artifactId>
        <version>1.0.0</version>
    </parent>
```

## Access to GetJabbed Maven Repository

### settings.xml

```
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                      http://maven.apache.org/xsd/settings-1.0.0.xsd">

    <activeProfiles>
        <activeProfile>main</activeProfile>
    </activeProfiles>

    <profiles>
        <profile>
            <id>main</id>
            <repositories>
                <repository>
                    <id>central</id>
                    <url>https://repo1.maven.org/maven2</url>
                </repository>
                <repository>
                    <id>gitlab-maven</id>
                    <url>https://gitlab.com/api/v4/groups/51789812/-/packages/maven</url>
                </repository>
            </repositories>
        </profile>
    </profiles>

    <servers>
        <server>
            <id>gitlab-maven</id>
            <configuration>
                <httpHeaders>
                    <property>
                        <name>Private-Token</name>
                        <value>###PUT HERE PERSONAL ACCESS TOKEN###</value>
                    </property>
                </httpHeaders>
            </configuration>
        </server>
    </servers>
</settings>
```

You can generate personal access token here: https://gitlab.com/-/profile/personal_access_tokens